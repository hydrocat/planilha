package com.example.wordlist

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_add_word.*


class AddWord : AppCompatActivity() {

    companion object {
        const val EXTRA_NEW_WORD = "NEW_WORD"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_word)

        button_save.setOnClickListener {
            val replyIntent = Intent()

            if ( edit_word.text.isEmpty() ) {
                setResult(Activity.RESULT_CANCELED)
                Log.d("ADD", "No Word")
            } else {
                replyIntent.putExtra(EXTRA_NEW_WORD, edit_word.text.toString())
                setResult(Activity.RESULT_OK, replyIntent)
                Log.d("ADD", "New word: ${edit_word.text}")
            }
            finish()
        }
    }
}