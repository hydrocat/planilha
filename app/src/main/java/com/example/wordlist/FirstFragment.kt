package com.example.wordlist

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wordlist.word.Word
import com.example.wordlist.word.data.WordViewModel
import com.example.wordlist.word.ui.WordListAdapter
import kotlinx.android.synthetic.main.fragment_first.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment() : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        Log.d("FirstFrag", "Fui Criado")
        val adapter = WordListAdapter()
        val wordVM : WordViewModel by activityViewModels()
        wordVM.allWords.observe(this.viewLifecycleOwner, object : Observer<List<Word>> {
            override fun onChanged(words: List<Word>?) {
                adapter.words = words
            }
        })

        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(context)

        super.onViewCreated(view, savedInstanceState)

//        recyclerview.adapter = WordListAdapter()
//        recyclerview.layoutManager = LinearLayoutManager(this.requireContext())
    }
}