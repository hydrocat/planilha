package com.example.wordlist.word

import androidx.room.*

@Entity(tableName = "word_table")
class Word(
    @PrimaryKey
    @ColumnInfo(name = "word", defaultValue = "Nothing")
    var word: String)