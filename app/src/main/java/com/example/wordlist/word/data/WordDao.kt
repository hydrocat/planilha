package com.example.wordlist.word.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.wordlist.word.Word

@Dao
interface WordDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(word: Word)

    @Delete
    fun delete(word: Word)

    @Query("DELETE FROM word_table")
    fun deleteAll()

    @Query("SELECT * FROM word_table ORDER BY word ASC")
    fun getAllWords() : LiveData<List<Word>>
}