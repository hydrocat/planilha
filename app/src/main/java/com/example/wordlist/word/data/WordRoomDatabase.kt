package com.example.wordlist.word.data

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.wordlist.word.Word

@Database(entities = [Word::class], version = 1, exportSchema = false)
abstract class WordRoomDatabase : RoomDatabase() {

    companion object Factory {
        private var mInstance: WordRoomDatabase? = null

        private val sRoomDatabaseCallback: RoomDatabase.Callback =
            object : RoomDatabase.Callback() {
                override fun onOpen(db: SupportSQLiteDatabase) {
                    super.onOpen(db)
                    PopulateDbAsync(mInstance!!).execute()
                }
            }

        fun getDatabase(context: Context): WordRoomDatabase {
            if (mInstance == null) {
                synchronized(WordRoomDatabase::class) {
                    mInstance = Room.databaseBuilder(
                        context.applicationContext,
                        WordRoomDatabase::class.java, "word_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(sRoomDatabaseCallback)
                        .build()
                }
            }

            return mInstance!!
        }

    }

    abstract fun wordDao(): WordDao

    class PopulateDbAsync(var db: WordRoomDatabase) : AsyncTask<Void, Void, Void>() {

        private var mDao: WordDao = db.wordDao()
        private val mWords = listOf<String>("abobora", "berinjela", "carnaúba")

        override fun doInBackground(vararg params: Void?): Void? {
            mDao.deleteAll()
            mWords.forEach {
                mDao.insert(Word(it))
            }

            return null
        }
    }
}
