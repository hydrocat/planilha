package com.example.wordlist.word.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.wordlist.word.Word

class WordViewModel(application: Application) : AndroidViewModel(application) {

    private val mWordRepository: WordRepository = WordRepository(application)

    val allWords: LiveData<List<Word>>

    init {
        allWords = mWordRepository.getAllWords()
    }

    fun insert(word: Word) = mWordRepository.insert(word)

}