package com.example.wordlist.word.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wordlist.*
import com.example.wordlist.word.Word

class WordListAdapter() : RecyclerView.Adapter<WordListAdapter.WordListVH>() {

    var words: List<Word>? = null
    set(words){
        field = words
        notifyDataSetChanged()
    }

    class WordListVH(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordListVH {
        return WordListVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.recycler_view_item, parent, false
                )
        )
    }

    override fun getItemCount(): Int {
        return words?.size ?: 0
    }

    override fun onBindViewHolder(holder: WordListVH, position: Int) {
        holder
            .itemView
            .findViewById<TextView>(R.id.textView)
            .text = words?.get(position)?.word ?: "No Word"
    }

}